package net.aeronetwork.dd.game.state;

import club.encast.enflow.Enflow;
import club.encast.enflow.example.util.PlayerUtil;
import club.encast.enflow.example.util.Util;
import club.encast.enflow.game.Game;
import club.encast.enflow.game.state.GameState;
import club.encast.enflow.scoreboard.GScoreboard;
import club.encast.enflow.version.packet.out.title.PacketTitle;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.aeronetwork.dd.DungeonDwellers;
import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.entity.DungeonEntity;
import net.aeronetwork.dd.game.entity.DungeonEntityType;
import net.aeronetwork.dd.game.map.DDMap;
import net.aeronetwork.dd.game.player.GamePlayer;
import net.aeronetwork.dd.utils.PartialLocation;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PermanentState extends GameState {

    private DDGame game;
    private long elapsedTime;

    private Random random;
    private BukkitRunnable spectatorRunnable;

    public PermanentState() {
        super("Permanent", 10, TimeUnit.MINUTES, false);
        this.random = new Random();
        this.spectatorRunnable = new BukkitRunnable() {
            @Override
            public void run() {
                if(game != null) {
                    game.getSpectators().forEach(gp -> {
                        Player p = Bukkit.getServer().getPlayer(gp.getUuid());
                        if(p != null) {
                            long time = System.currentTimeMillis() - gp.getDeathTime();
                            Enflow.OPERATION_LIBRARY.getOperation(PacketTitle.class).send(
                                    p,
                                    " ",
                                    "§eYou will respawn in §c" +
                                            (time / 1000D) +
                                                    " seconds!",
                                    0,
                                    25,
                                    0
                            );

                            if(time >= TimeUnit.SECONDS.toMillis(5)) {
                                DDMap map = (DDMap) game.getMaps().get(0);
                                PartialLocation loc = map.getPlayerSpawnLocations()
                                        .get(random.nextInt(map.getPlayerSpawnLocations().size()));

                                PlayerUtil.clearInventory(p);
                                PlayerUtil.healPlayer(p);
                                gp.getKit().applyKit(p);
                                gp.setAlive(true);
                                p.teleport(game.parseLocation(loc));
                                p.setGameMode(GameMode.SURVIVAL);
                                p.sendMessage("§eYou have respawned!");
                            }
                        }
                    });
                }
            }
        };
        spectatorRunnable.runTaskTimer(DungeonDwellers.INSTANCE, 0, 5);
    }

    @Override
    public void onStateStart(Game game) {
    }

    @Override
    public void onStateTick(Game game) {
        if(this.game == null) {
            this.game = (DDGame) game;
        }
        if(game.getTimer().getCurrentState() instanceof RoundState) {
            this.elapsedTime++;
            RoundState state = (RoundState) game.getTimer().getCurrentState();

            if((System.currentTimeMillis() - state.getTempTime()) >= TimeUnit.SECONDS.toMillis(5) &&
                    state.getSpawnedEnemies() >= state.getEnemyCount() && state.getEnemiesLeft() == 0) {
                this.game.setRound(this.game.getCurrentRound() + 1);
            }

            this.game.getGamePlayers().forEach(gp -> {
                GScoreboard scoreboard = (GScoreboard) gp.getSessionObjects().get(GamePlayer.SESSION_SCOREBOARD_KEY);
                scoreboard.setDisplayName("§e§lDD - §c§lROUND " + this.game.getCurrentRound());
                scoreboard.setLine(1, "Balance: §e" + gp.getBalance());
                scoreboard.setLine(2, "Kills: §e" + gp.getKills());
                // Deprecated usage
                scoreboard.setLine(4, "Time Elapsed: §e" + Util.convertToMinuteSecond(this.elapsedTime));
            });

            // Telling players how to open the upgrade menu
            if(this.game.getCurrentRound() <= 2 && this.game.getTimer().getRemainingTime(TimeUnit.SECONDS) % 10 == 0) {
                this.game.sendMessage("§cSHIFT RIGHT-CLICK §eto open the upgrade menu!");
            }
        }
    }

    @Override
    public void onStateEnd(Game game) {

    }

    @Override
    public void onJoin(Player player) {

    }

    @Override
    public void onLeave(Player player) {

    }
}
