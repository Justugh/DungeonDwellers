package net.aeronetwork.dd.game.state;

import club.encast.enflow.Enflow;
import club.encast.enflow.game.Game;
import club.encast.enflow.game.state.GameState;
import club.encast.enflow.scoreboard.GScoreboard;
import club.encast.enflow.util.player.PlayerUtil;
import club.encast.enflow.version.packet.out.playerlist.PacketPlayerList;
import club.encast.enflow.version.packet.out.title.PacketTitle;
import net.aeronetwork.core.AeroCore;
import net.aeronetwork.core.player.AeroPlayer;
import net.aeronetwork.dd.DungeonDwellers;
import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.kit.kits.DefaultKit;
import net.aeronetwork.dd.game.listener.GameListener;
import net.aeronetwork.dd.game.map.DDMap;
import net.aeronetwork.dd.game.player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import java.util.concurrent.TimeUnit;

public class LobbyState extends GameState implements Listener {

    private DDGame game;
    private GScoreboard scoreboard;

    public LobbyState() {
        super("Lobby", 15, TimeUnit.SECONDS, true);

        this.scoreboard = new GScoreboard("§e§lDUNGEON DWELLERS")
                .addLine(" ")
                .addLine("Players: §ex")
                .addLine("  ")
                .addLine("Starting in §ex")
                .addLine("   ")
                .addLine("Map: §ex")
                .addLine("    ")
                .addLine("§cAero Network");

        Bukkit.getServer().getPluginManager().registerEvents(this, DungeonDwellers.INSTANCE);
    }

    @Override
    public void onStateStart(Game game) {
        this.game = (DDGame) game;
    }

    @Override
    public void onStateTick(Game game) {
        if(game.getPlayers().size() >= game.getSettings().getMaxPlayers() / 2) {
            if(isInfinite()) {
                setInfinite(false);
                game.getTimer().setCurrentLengthSeconds(0);
            }
        } else {
            if(!isInfinite()) {
                setInfinite(true);
                game.getTimer().setCurrentLengthSeconds(0);
            }
        }
        if(game.getTimer().getCurrentLengthSeconds() % 5 == 0 && isInfinite()) {
            this.game.getAllPlayers(true).forEach(p ->
                    Enflow.OPERATION_LIBRARY.getOperation(PacketTitle.class).send(
                            p,
                            " ",
                            "§eThe game will start shortly!",
                            10,
                            25,
                            10
                    ));
        } else {
            this.game.getAllPlayers(true).forEach(p ->
                    Enflow.OPERATION_LIBRARY.getOperation(PacketTitle.class).send(
                            p,
                            " ",
                            "§eThe game will start in §c" + game.getTimer().getRemainingTime(TimeUnit.SECONDS) + "s",
                            10,
                            25,
                            10
                    ));
        }

        this.scoreboard.setLine(1, "Players: §e" + game.getPlayers().size());
        this.scoreboard.setLine(3, "Starting in: §e" + (isInfinite() ? "§cSoon" :
                game.getTimer().getRemainingTime(TimeUnit.SECONDS) + "s"));
        this.scoreboard.setLine(5, "Map: §e" + this.game.getMaps().get(0).getMapName());
    }

    @Override
    public void onStateEnd(Game game) {
        this.game.getAllPlayers(true).forEach(player -> {
            GamePlayer gp = new GamePlayer(player.getUniqueId());
            GScoreboard scoreboard = new GScoreboard("§e§lDUNGEON DWELLERS")
                    .addLine(" ")
                    .addLine("Balance: x")
                    .addLine("Kills: ")
                    .addLine("  ")
                    .addLine("Time Elapsed: x");
            gp.getSessionObjects().put("scoreboard", scoreboard);
            this.game.getGamePlayers().add(gp);

            gp.setKit(new DefaultKit());

            gp.getKit().applyKit(player);
            scoreboard.setScoreboard(player);
            player.sendMessage("§aSelected the default kit!");
        });

        RoundState tempState = new RoundState();
        tempState.getSpawnedEntities().clear();
        game.getTimer().setCurrentState(tempState);

        Bukkit.getServer().getPluginManager().registerEvents(new GameListener(this.game), DungeonDwellers.INSTANCE);
        HandlerList.unregisterAll(this);
    }

    @Override
    public void onJoin(Player player) {
        AeroPlayer ap = AeroCore.PLAYER_MANAGER.getPlayer(player.getUniqueId());
        this.game.getAllPlayers(true).forEach(p ->
            p.sendMessage("§8(§c" + this.game.getPlayers().size() + "§8/§c" +
                    this.game.getSettings().getMaxPlayers() + "§8) > " +
                    ap.getRank().getColor() + player.getName() + " joined the game."));

        Enflow.OPERATION_LIBRARY.getOperation(PacketPlayerList.class).send(
                player,
                "§7You are playing §e§lDUNGEON DWELLERS",
                "\n§c§lAero Network\n"
        );

        player.teleport(this.game.parseLocation(((DDMap)
                this.game.getMaps().get(0)).getPlayerSpawnLocations().get(0)));

        PlayerUtil.clearInventory(player);
        PlayerUtil.heal(player);

        scoreboard.setScoreboard(player);
    }

    @Override
    public void onLeave(Player player) {
        this.game.getAllPlayers(true).forEach(p ->
            p.sendMessage("§8(§c" + this.game.getPlayers().size() + "§8/§c" +
                    this.game.getSettings().getMaxPlayers() + "§8) > §7" +
                    player.getName() + " left the game."));
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setFoodLevel(20);
        e.setCancelled(true);
    }
}
