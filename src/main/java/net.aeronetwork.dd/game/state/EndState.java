package net.aeronetwork.dd.game.state;

import club.encast.enflow.game.Game;
import club.encast.enflow.game.state.GameState;
import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.player.GamePlayer;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class EndState extends GameState {

    private DDGame game;

    public EndState() {
        super("End", 25, TimeUnit.SECONDS, false);
    }

    @Override
    public void onStateStart(Game game) {
        this.game = (DDGame) game;
        AtomicInteger totalKills = new AtomicInteger(0);
        this.game.getGamePlayers().forEach(gp -> {
            totalKills.addAndGet(gp.getKills());
        });
        this.game.getAllPlayers(true).forEach(p -> {
            GamePlayer gp = this.game.getGamePlayer(p.getUniqueId());
            p.sendMessage("§c§lTHE GAME HAS ENDED!");
            p.sendMessage("§eYou and your team got to round " + this.game.getCurrentRound() + "!");
            p.sendMessage("§e> Your team killed " + totalKills.get() + " enemy/enemies!");
            p.sendMessage("§e> You ended with " + gp.getBalance() + " coin(s)!");
        });
    }

    @Override
    public void onStateTick(Game game) {

    }

    @Override
    public void onStateEnd(Game game) {
        this.game.getAllPlayers(true).forEach(p -> p.kickPlayer("§cGame has ended!"));
    }

    @Override
    public void onJoin(Player player) {
        player.sendMessage("§cThe game has ended!");
    }

    @Override
    public void onLeave(Player player) {

    }
}
