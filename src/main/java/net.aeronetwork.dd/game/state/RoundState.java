package net.aeronetwork.dd.game.state;

import club.encast.enflow.Enflow;
import club.encast.enflow.game.Game;
import club.encast.enflow.game.map.GameMap;
import club.encast.enflow.game.state.GameState;
import club.encast.enflow.util.Tuple;
import club.encast.enflow.version.packet.out.title.PacketTitle;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.entity.DungeonEntity;
import net.aeronetwork.dd.game.entity.impl.ZombieEntity;
import net.aeronetwork.dd.game.map.DDMap;
import net.aeronetwork.dd.utils.PartialLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
public class RoundState extends GameState {

    private int round;
    private Map<DungeonEntity, Integer> validEnemies = Maps.newHashMap();

    private transient int enemyCount = 0;
    private transient int spawnedEnemies = 0;
    private transient List<Tuple<DungeonEntity, List<Entity>>> spawnedEntities = Lists.newArrayList();
    private transient long tempTime = System.currentTimeMillis();

    private Random random;

    private DDGame game;

    public RoundState() {
        super("Round N/A", 10, TimeUnit.MINUTES, true);

        this.random = new Random();

        calcTotalEnemies();
    }

    public Tuple<DungeonEntity, List<Entity>> getSpawnedEntity(DungeonEntity entity) {
        return this.spawnedEntities.stream()
                .filter(tuple -> tuple.getObjectA() == entity)
                .findFirst()
                .orElse(null);
    }

    public void setValidEnemies(Map<DungeonEntity, Integer> validEnemies) {
        this.validEnemies = validEnemies;

        calcTotalEnemies();
    }

    public boolean isSpawnable(DungeonEntity entity) {
        int max = validEnemies.getOrDefault(entity, -1);
        if(max != -1) {
            Tuple<DungeonEntity, List<Entity>> tuple = getSpawnedEntity(entity);
            if(tuple != null) {
                if(tuple.getObjectB().size() < max) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public DungeonEntity getDungeonEntity(Entity entity) {
        CompletableFuture<DungeonEntity> de = new CompletableFuture<>();
        this.spawnedEntities.forEach(spawned -> {
            if(spawned != null && spawned.getObjectB().contains(entity))
                de.complete(spawned.getObjectA());
        });
        return de.getNow(null);
    }

    public void calcTotalEnemies() {
        AtomicInteger count = new AtomicInteger();
        this.validEnemies.forEach((entity, amount) -> count.addAndGet(amount));
        this.enemyCount = count.get();
    }

    private void addEntity(DungeonEntity dungeonEntity, Entity entity) {
        boolean added = false;
        for(Tuple<DungeonEntity, List<Entity>> tuple : this.spawnedEntities) {
            if(tuple.getObjectA().getClass() == dungeonEntity.getClass()) {
                tuple.getObjectB().add(entity);
                added = true;
            }
        }

        if(!added) {
            this.spawnedEntities.add(new Tuple<>(dungeonEntity, Lists.newArrayList(entity)));
        }
    }

    public int getEnemiesLeft() {
        AtomicInteger enemiesLeft = new AtomicInteger(0);

        spawnedEntities.forEach(tuple -> {
            if(tuple.getObjectB() != null)
                tuple.getObjectB().forEach(entity -> {
                    if(entity.isValid())
                        enemiesLeft.addAndGet(1);
                });
        });

        return enemiesLeft.get();
    }

    @Override
    public void onStateStart(Game game) {
        this.game = (DDGame) game;

        this.game.getAllPlayers(true).forEach(p ->
            Enflow.OPERATION_LIBRARY.getOperation(PacketTitle.class).send(
                    p,
                    "§c§lROUND " + getRound() + "!",
                    "Enemy Count: " + getEnemyCount(),
                    10,
                    25,
                    10
            ));
    }

    @Override
    public void onStateTick(Game game) {
        if(spawnedEnemies < this.enemyCount) {
            int toSpawn = this.random.nextInt(3) + 1;
            int spawned = 0;
            List<Map.Entry<DungeonEntity, Integer>> entities = Lists.newCopyOnWriteArrayList(validEnemies.entrySet());
            for(Map.Entry<DungeonEntity, Integer> entry : entities) {
                if(!isSpawnable(entry.getKey()))
                    entities.remove(entry);
            }
            Collections.shuffle(entities);

            DDMap map = (DDMap) this.game.getMaps().get(0);

            do {
                for(Map.Entry<DungeonEntity, Integer> entry : entities) {
                    if(spawned <= toSpawn) {
                        PartialLocation loc = map.getZombieSpawnLocations()
                                .get(random.nextInt(map.getZombieSpawnLocations().size()));
                        Entity entity = entry.getKey().spawn(this.game.parseLocation(loc));
                        addEntity(entry.getKey(), entity);
                        spawned++;
                    }
                }
            } while(entities.size() >= 1 && spawned < toSpawn && getSpawnedEnemies() < getEnemyCount());
            this.spawnedEnemies += spawned;
        }
    }

    @Override
    public void onStateEnd(Game game) {

    }

    @Override
    public void onJoin(Player player) {

    }

    @Override
    public void onLeave(Player player) {

    }
}
