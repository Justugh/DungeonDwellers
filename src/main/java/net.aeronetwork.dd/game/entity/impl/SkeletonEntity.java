package net.aeronetwork.dd.game.entity.impl;

import net.aeronetwork.dd.game.entity.DungeonEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class SkeletonEntity extends DungeonEntity {

    public SkeletonEntity() {
        super("§7Skeleton", EntityType.SKELETON, 0, 20, 1, false, 15);
    }

    @Override
    public void onSpawn(Entity entity) {

    }
}
