package net.aeronetwork.dd.game.entity.impl;

import net.aeronetwork.dd.game.entity.DungeonEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class EvolvedSkeletonEntity extends DungeonEntity {

    public EvolvedSkeletonEntity() {
        super("§cEvolved Skeleton", EntityType.SKELETON, 0, 50, 2, false, 100);
    }

    @Override
    public void onSpawn(Entity entity) {

    }
}
