package net.aeronetwork.dd.game.entity.impl;

import net.aeronetwork.dd.game.entity.DungeonEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class ZombieEntity extends DungeonEntity {

    public ZombieEntity() {
        super("Zombie", EntityType.ZOMBIE, 0, 10, 0, false, 10);
    }

    @Override
    public void setLevel(int level) {
        super.setLevel(level);
        super.setHealth(10 * level);
    }

    @Override
    public void onSpawn(Entity entity) {

    }
}
