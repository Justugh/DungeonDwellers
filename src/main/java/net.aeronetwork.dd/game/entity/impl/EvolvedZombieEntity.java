package net.aeronetwork.dd.game.entity.impl;

import net.aeronetwork.dd.game.entity.DungeonEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class EvolvedZombieEntity extends DungeonEntity {

    public EvolvedZombieEntity() {
        super("§cEvolved Zombie", EntityType.ZOMBIE, 0, 25, 1, false, 25);
    }

    @Override
    public void onSpawn(Entity entity) {
    }
}
