package net.aeronetwork.dd.game.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.aeronetwork.dd.game.entity.impl.EvolvedSkeletonEntity;
import net.aeronetwork.dd.game.entity.impl.EvolvedZombieEntity;
import net.aeronetwork.dd.game.entity.impl.SkeletonEntity;
import net.aeronetwork.dd.game.entity.impl.ZombieEntity;

@AllArgsConstructor
@Getter
public enum DungeonEntityType {

    ZOMBIE(ZombieEntity.class, 1),
    ZOMBIE_EVOLVED(EvolvedZombieEntity.class, 5),
    SKELETON(SkeletonEntity.class, 10),
    SKELETON_EVOLVED(EvolvedSkeletonEntity.class, 20);

    private Class<? extends DungeonEntity> entityClass;
    private int minRound;
}
