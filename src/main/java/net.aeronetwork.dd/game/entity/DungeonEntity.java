package net.aeronetwork.dd.game.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@AllArgsConstructor
@Getter
@Setter
public abstract class DungeonEntity {

    private String name;
    private EntityType entityType;
    private int level;
    private double health;
    private int speed;
    private boolean boss;
    private long killCoins;

    public final Entity spawn(Location location) {
        Entity entity = location.getWorld().spawnEntity(location, entityType);
        entity.setCustomName(name);
        entity.setCustomNameVisible(false);

        if(entity instanceof LivingEntity) {
            LivingEntity livingEntity = (LivingEntity) entity;
            livingEntity.setMaxHealth(health);
            if(speed >= 1)
                livingEntity.addPotionEffect(new PotionEffect(
                        PotionEffectType.SPEED,
                        1_000_000,
                        speed,
                        true,
                        false)
                );
        }

        onSpawn(entity);
        return entity;
    }

    public abstract void onSpawn(Entity entity);
}
