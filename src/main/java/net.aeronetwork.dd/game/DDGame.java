package net.aeronetwork.dd.game;

import club.encast.enflow.example.util.ServerPropertiesUtil;
import club.encast.enflow.game.Game;
import club.encast.enflow.game.handler.GameHandler;
import club.encast.enflow.game.settings.GameSettings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import net.aeronetwork.dd.game.entity.DungeonEntity;
import net.aeronetwork.dd.game.entity.DungeonEntityType;
import net.aeronetwork.dd.game.map.impl.CavernMap;
import net.aeronetwork.dd.game.player.GamePlayer;
import net.aeronetwork.dd.game.state.LobbyState;
import net.aeronetwork.dd.game.state.PermanentState;
import net.aeronetwork.dd.game.state.RoundState;
import net.aeronetwork.dd.utils.PartialLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
public class DDGame extends Game {

    private List<GamePlayer> gamePlayers;
    @Setter
    private int currentRound = 0;
    private World gameWorld;

    public DDGame(GameHandler handler) {
        super(handler);

        setSettings(new GameSettings());
        getSettings().setMaxPlayers(2);

        this.gamePlayers = Lists.newArrayList();

        this.getMaps().add(new CavernMap());

        getGameStates().add(LobbyState.class);
        setPermanentState(new PermanentState());

        this.gameWorld = Bukkit.getServer().getWorld("dungeon_dwellers");
        this.gameWorld.setGameRuleValue("doMobLoot", "false");
    }

    public List<Player> getAllPlayers(boolean filter) {
        return getPlayers().stream()
                .map(uuid -> Bukkit.getServer().getPlayer(uuid))
                .filter(player -> !filter || player != null)
                .collect(Collectors.toList());
    }

    public List<GamePlayer> getAlivePlayers() {
        return this.gamePlayers.stream()
                .filter(GamePlayer::isAlive)
                .collect(Collectors.toList());
    }

    public List<GamePlayer> getSpectators() {
        return this.gamePlayers.stream()
                .filter(gp -> !gp.isAlive())
                .collect(Collectors.toList());
    }

        public GamePlayer getGamePlayer(UUID uuid) {
        return this.gamePlayers.stream()
                .filter(gp -> gp.getUuid() == uuid)
                .findFirst()
                .orElse(null);
    }

    public Location parseLocation(PartialLocation location) {
        return new Location(
                this.gameWorld,
                location.getX(),
                location.getY(),
                location.getZ(),
                location.getYaw(),
                location.getPitch()
        );
    }

    public void setRound(int round) {
        try {
            int quota = (getCurrentRound() + 1) * 5;
            int current = 0;
            Map<DungeonEntity, Integer> enemies = Maps.newHashMap();

            List<DungeonEntity> roundEnemies = Lists.newArrayList();
            for (DungeonEntityType type : DungeonEntityType.values()) {
                if (((getCurrentRound() + 1) >= type.getMinRound())) {
                    if (type.getEntityClass() != null)
                        roundEnemies.add(type.getEntityClass().newInstance());
                }
            }
            for (DungeonEntity entity : roundEnemies) {
                if (current < quota) {
                    // Spawn all possible mobs at least once
                    enemies.put(entity, 1);
                    current++;
                }
            }

            // Shuffle up enemies to give more randomness (only executed if the quota isn't met)
            if (current < quota) {
                List<DungeonEntity> shuffledEnemies = Lists.newArrayList(roundEnemies);
                Collections.shuffle(shuffledEnemies);

                do {
                    for (DungeonEntity entity : shuffledEnemies) {
                        if (!enemies.containsKey(entity)) {
                            enemies.put(entity, 1);
                        } else {
                            int count = enemies.get(entity);
                            enemies.replace(entity, count, count + 1);
                        }
                        current++;
                    }
                } while (current < quota);
            }

            RoundState newState = new RoundState();
            newState.setName("Round " + (getCurrentRound() + 1));
            newState.setRound(getCurrentRound() + 1);
            newState.setValidEnemies(enemies);

            setCurrentRound(round);

            newState.onStateStart(this);
            getTimer().setCurrentState(newState);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        this.getAllPlayers(true).forEach(p -> p.sendMessage(message));
    }
}
