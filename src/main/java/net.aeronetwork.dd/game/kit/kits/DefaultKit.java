package net.aeronetwork.dd.game.kit.kits;

import net.aeronetwork.dd.game.kit.Kit;
import net.aeronetwork.dd.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DefaultKit extends Kit {

    public DefaultKit() {
        super("Miner", new String[]{"Default miner kit"}, new ItemStack(Material.WOOD_PICKAXE), 0);
    }

    @Override
    public void applyKit(Player player) {
        player.getInventory().setArmorContents(new ItemStack[]{new ItemStackBuilder("Leather Boots").setMaterial(Material.LEATHER_BOOTS).build().setUnbreakable(true).get(),
                new ItemStackBuilder("Leather Leggings").setMaterial(Material.LEATHER_LEGGINGS).build().setUnbreakable(true).get(),
                new ItemStackBuilder("Leather Chestplate").setMaterial(Material.LEATHER_CHESTPLATE).build().setUnbreakable(true).get(),
                new ItemStackBuilder("Leather Helmet").setMaterial(Material.LEATHER_HELMET).build().setUnbreakable(true).get()});
        player.getInventory().setItem(0, new ItemStackBuilder("Miner's Pickaxe").setMaterial(Material.STONE_PICKAXE).build().setUnbreakable(true).get());
    }
}
