package net.aeronetwork.dd.game.kit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@AllArgsConstructor
@Getter
@Setter
public abstract class Kit {

    private String name;
    private String[] description;
    private ItemStack item;
    private int cost;

    public abstract void applyKit(Player player);
}
