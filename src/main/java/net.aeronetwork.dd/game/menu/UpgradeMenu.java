package net.aeronetwork.dd.game.menu;

import club.encast.enflow.Enflow;
import club.encast.enflow.inventory.ClickData;
import club.encast.enflow.inventory.EnflowInventory;
import club.encast.enflow.inventory.InvSize;
import club.encast.enflow.item.ItemBuilder;
import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.player.GamePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class UpgradeMenu extends EnflowInventory {

    public UpgradeMenu() {
        super("Upgrades", InvSize.SIX_ROWS);

        addItem(ItemBuilder.newBuilder(Material.GOLDEN_APPLE, ItemBuilder.ItemType.NORMAL)
                .setDisplayName("§6Buy Golden Apple")
                .addLore(" ", "§ePrice: 250 coins")
                .build(),
                data -> {
                    GamePlayer player = ((DDGame) Enflow.GAME_MANAGER.getGame(data.getPlayer()))
                            .getGamePlayer(data.getPlayer().getUniqueId());
                    if(player != null) {
                        if(player.getBalance() >= 250) {
                            player.setBalance(player.getBalance() - 250);
                            data.getPlayer().sendMessage("§aPurchased Golden Apple for 250 coins!");
                            data.getPlayer().getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
                        } else {
                            data.getPlayer().sendMessage("§cYou don't have enough coins to purchase this!");
                        }
                    }
                }
        );

        addItem(ItemBuilder.newBuilder(Material.GOLDEN_APPLE, ItemBuilder.ItemType.NORMAL)
                        .setDisplayName("§6Buy Diamond Sword")
                        .addLore(" ", "§ePrice: 1000 coins")
                        .build(),
                data -> {
                    GamePlayer player = ((DDGame) Enflow.GAME_MANAGER.getGame(data.getPlayer()))
                            .getGamePlayer(data.getPlayer().getUniqueId());
                    if(player != null) {
                        if(player.getBalance() >= 1000) {
                            player.setBalance(player.getBalance() - 1000);
                            data.getPlayer().sendMessage("§aPurchased Diamond Sword for 1000 coins!");
                            data.getPlayer().getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
                        } else {
                            data.getPlayer().sendMessage("§cYou don't have enough coins to purchase this!");
                        }
                    }
                }
        );
    }

    @Override
    public void onOpen(Player player) {

    }

    @Override
    public void onClick(ClickData clickData) {

    }

    @Override
    public void onClose(Player player) {

    }
}
