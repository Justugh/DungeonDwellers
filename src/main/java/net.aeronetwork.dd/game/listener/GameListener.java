package net.aeronetwork.dd.game.listener;

import net.aeronetwork.dd.game.DDGame;
import net.aeronetwork.dd.game.entity.DungeonEntity;
import net.aeronetwork.dd.game.map.DDMap;
import net.aeronetwork.dd.game.menu.UpgradeMenu;
import net.aeronetwork.dd.game.player.GamePlayer;
import net.aeronetwork.dd.game.state.EndState;
import net.aeronetwork.dd.game.state.RoundState;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class GameListener implements Listener {

    private DDGame game;

    public GameListener(DDGame game) {
        this.game = game;
    }

    @EventHandler
    public void onEntityKill(EntityDeathEvent e) {
        if(!(e.getEntity() instanceof Player) && e.getEntity().getKiller() != null) {
            Player p = e.getEntity().getKiller();
            GamePlayer gp = this.game.getGamePlayer(p.getUniqueId());
            if(gp != null) {
                RoundState state = (RoundState) this.game.getTimer().getCurrentState();
                DungeonEntity entity = state.getDungeonEntity(e.getEntity());

                if(entity != null) {
                    gp.setKills(gp.getKills() + 1);
                    gp.setBalance(gp.getBalance() + entity.getKillCoins());
                    p.sendMessage("§e+" + entity.getKillCoins() + " Coin(s)!");
                }
                e.setDroppedExp(0);
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        GamePlayer player = this.game.getGamePlayer(p.getUniqueId());
        p.setGameMode(GameMode.SPECTATOR);
        if(player != null) {
            player.setAlive(false);
            player.setDeathTime(System.currentTimeMillis());
            p.sendMessage("§cYou will respawn in 5 seconds.");
            if(game.getAlivePlayers().size() == 0) {
                this.game.getTimer().setCurrentState(new EndState());
                this.game.getTimer().getCurrentState().onStateStart(this.game);
            }
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        e.getPlayer().teleport(this.game.parseLocation(((DDMap)
                this.game.getMaps().get(0)).getPlayerSpawnLocations().get(0)));
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setFoodLevel(20);
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            if(e.getCause() == EntityDamageEvent.DamageCause.FALL)
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getAction() == Action.RIGHT_CLICK_AIR && e.getPlayer().isSneaking()) {
            UpgradeMenu menu = new UpgradeMenu();
            menu.openInventory(e.getPlayer());
        }
    }
}
