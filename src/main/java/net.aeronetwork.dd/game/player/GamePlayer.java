package net.aeronetwork.dd.game.player;

import com.google.common.collect.Maps;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.aeronetwork.dd.game.kit.Kit;

import java.util.Map;
import java.util.UUID;

@Getter @Setter
public class GamePlayer {

    @Setter(AccessLevel.NONE)
    private UUID uuid;
    private int kills;
    private long balance;
    private Kit kit;

    private boolean alive = true;
    private long deathTime;

    private Map<String, Object> sessionObjects = Maps.newConcurrentMap();

    public static String SESSION_SCOREBOARD_KEY = "scoreboard";

    public GamePlayer(UUID uuid) {
        this.uuid = uuid;
    }
}
