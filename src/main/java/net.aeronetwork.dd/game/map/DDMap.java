package net.aeronetwork.dd.game.map;

import club.encast.enflow.game.map.GameMap;
import com.google.common.collect.Lists;
import lombok.Getter;
import net.aeronetwork.dd.utils.PartialLocation;

import java.util.List;

@Getter
public abstract class DDMap extends GameMap {

    private List<PartialLocation> playerSpawnLocations;
    private List<PartialLocation> zombieSpawnLocations;

    public DDMap(String mapName, List<String> builders) {
        super(mapName, builders, null);

        this.playerSpawnLocations = Lists.newArrayList();
        this.zombieSpawnLocations = Lists.newArrayList();
    }
}
