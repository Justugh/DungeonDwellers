package net.aeronetwork.dd.game.map.impl;

import com.google.common.collect.Lists;
import net.aeronetwork.dd.game.map.DDMap;
import net.aeronetwork.dd.utils.PartialLocation;

public class CavernMap extends DDMap {

    public CavernMap() {
        super("Cavern", Lists.newArrayList());

        getPlayerSpawnLocations().add(new PartialLocation(-491, 44, -703, 0, 0));
        getZombieSpawnLocations().add(new PartialLocation(-503, 44, -724, 0, 0));
        getZombieSpawnLocations().add(new PartialLocation(-473, 44, -734, 0, 0));
    }
}
