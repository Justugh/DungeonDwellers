package net.aeronetwork.dd.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PartialLocation {

    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;
}
