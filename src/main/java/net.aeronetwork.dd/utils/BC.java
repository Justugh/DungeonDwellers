package net.aeronetwork.dd.utils;

import org.bukkit.ChatColor;

public class BC {

    //Basic Colors
    public static String White = ChatColor.WHITE.toString();
    public static String Black = ChatColor.BLACK.toString();
    public static String Aqua = ChatColor.AQUA.toString();
    public static String Blue = ChatColor.BLUE.toString();
    public static String Gray = ChatColor.GRAY.toString();
    public static String Green = ChatColor.GREEN.toString();
    public static String Red = ChatColor.RED.toString();
    public static String Yellow = ChatColor.YELLOW.toString();
    public static String Gold = ChatColor.GOLD.toString();

    //Light Colors
    public static String LightPurple = ChatColor.LIGHT_PURPLE.toString();

    //Dark Colors
    public static String DarkBlue = ChatColor.DARK_BLUE.toString();
    public static String DarkPurple = ChatColor.DARK_PURPLE.toString();
    public static String DarkGray = ChatColor.DARK_GRAY.toString();
    public static String DarkGreen = ChatColor.DARK_GREEN.toString();
    public static String DarkAqua = ChatColor.DARK_AQUA.toString();
    public static String DarkRed = ChatColor.DARK_RED.toString();

    //Text effects
	public static String Magic = ChatColor.MAGIC.toString();
	public static String Bold = ChatColor.BOLD.toString();
	public static String Strikethrough = ChatColor.STRIKETHROUGH.toString();
	public static String Underline = ChatColor.UNDERLINE.toString();
	public static String Italic = ChatColor.ITALIC.toString();

    /**
     * Translate color coding.
     *
     * @param text - Text to be converted.
     * @return String - Converted text.
     */
    public static String translate(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }

}
