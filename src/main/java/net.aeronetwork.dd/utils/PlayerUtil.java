package net.aeronetwork.dd.utils;

import net.aeronetwork.dd.game.player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerUtil {

    public static void setAsSpectator(GamePlayer player) {
        Player bukkitPlayer = Bukkit.getPlayer(player.getUuid());
        player.setAlive(false);
        bukkitPlayer.setGameMode(GameMode.SURVIVAL);
        reset(bukkitPlayer);
        bukkitPlayer.setAllowFlight(true);
        for(Player other : Bukkit.getOnlinePlayers()) {
            other.hidePlayer(bukkitPlayer);
        }
    }

    public static void reset(Player player) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[] {new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        player.setTotalExperience(0);
        player.setAllowFlight(false);
    }

}
