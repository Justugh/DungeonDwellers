package net.aeronetwork.dd.utils;

import org.bukkit.ChatColor;

public class FM {
	
	public static String mainFormat(String body){
		return BC.DarkRed + BC.Bold + "Aero Network " + BC.DarkGray + "» " + BC.Yellow + body;
	}
	
	public static String mainFormat(String module, String body){
		return BC.DarkRed + BC.Bold + module + BC.DarkGray + " » " + BC.Yellow + ChatColor.translateAlternateColorCodes('&', body);
	}

}
