package net.aeronetwork.dd.utils;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class UtilPacket {

	public static void sendTab(Player user, String header, String footer) {
		PlayerConnection playerConnection = ((CraftPlayer) user).getHandle().playerConnection;
		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();

        try {
			Field headerField = packet.getClass().getDeclaredField("a");
			headerField.setAccessible(true);
			headerField.set(packet, new ChatComponentText(header));

			Field footerField = packet.getClass().getDeclaredField("b");
			footerField.setAccessible(true);
			footerField.set(packet, new ChatComponentText(footer));

            headerField.setAccessible(false);
            footerField.setAccessible(false);
		} catch (IllegalAccessException exception) {
            System.out.println("[DD] Could not access packet methods.");
            exception.printStackTrace();
		} catch (NoSuchFieldException exception) {
            System.out.println("[DD] Invalid packet fields.");
            exception.printStackTrace();
        }

        playerConnection.sendPacket(packet);
	}

	public static void sendTitle(Player player, String title, String subtitle, Integer seen) {
//		player.sendTitle(title, subtitle, seen, 1, 1);
	}

	public static void sendActionBar(Player player, String message) {
//		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new ComponentBuilder(message).create());
	}

}