package net.aeronetwork.dd.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.HashSet;

public class ItemStackBuilder {
	
	private String name = "Item";
	private HashSet<String> lore = new HashSet<>();
	private Material material = Material.LOG;
	private short data = 0;
	private int amount = 1;
	private ItemStack stack;
	
    public ItemStackBuilder(String name){
        this.name = name;
    }
    
    public ItemStackBuilder(String name, Material material){
        this.name = name;
        this.material = material;
    }
	
	public ItemStackBuilder setName(String name){
		this.name = name;
		return this;
	}
	
	public ItemStackBuilder setLore(String[] lore){
		ItemMeta meta = stack.getItemMeta();
		meta.setLore(Arrays.asList(lore));
		stack.setItemMeta(meta);
		return this;
	}
	
	public ItemStackBuilder setMaterial(Material material){
		this.material = material;
		return this;
	}
	
	public ItemStackBuilder setUnbreakable(boolean unbreakable){
		stack.getItemMeta().spigot().setUnbreakable(unbreakable);
		return this;
	}
	
	public ItemStackBuilder setAmount(int amount){
		if(amount < 1){
			this.amount = 1;
		}else{
			this.amount = amount;
		}
		return this;
	}
	
	public ItemStackBuilder setData(short data){
		this.data = data;
		return this;
	}
	
	public ItemStackBuilder addEnchant(Enchantment enchant, int level){
		stack.addUnsafeEnchantment(enchant, level);
		return this;
	}
	
	public ItemStackBuilder removeEnchant(Enchantment enchant){
		stack.removeEnchantment(enchant);
		return this;
	}
	
    public ItemStackBuilder build(){
		stack = new ItemStack(material, amount, data);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(BC.translate(BC.White + name));
		meta.setLore(Arrays.asList(lore.toArray(new String[] {})));
		stack.setItemMeta(meta);
		return this;
	}
	
	public ItemStack get(){
		return stack;
	}
	
	public ItemStackBuilder asSkull(String player){
		stack = new ItemStack(Material.SKULL_ITEM, amount, (short) 3);
		SkullMeta meta = (SkullMeta) stack.getItemMeta();
		meta.setDisplayName(BC.translate(BC.White + name));
		meta.setLore(Arrays.asList(lore.toArray(new String[] {})));
		meta.setOwner(player);
		stack.setItemMeta(meta);
		return this;
	}

}
