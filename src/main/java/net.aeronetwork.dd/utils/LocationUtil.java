package net.aeronetwork.dd.utils;

import org.bukkit.Location;

import java.util.Random;

public class LocationUtil {

    public static Location getRandomLocation(Location origin, double radius)
    {
        Random r = new Random();
        double randomRadius = r.nextDouble() * radius;
        double theta =  Math.toRadians(r.nextDouble() * 360);
        double phi = Math.toRadians(r.nextDouble() * 180 - 90);

        double x = randomRadius * Math.cos(theta) * Math.sin(phi);
        double z = randomRadius * Math.cos(phi);
        Location newLoc = origin.clone().add(x, 0, z);

        return newLoc;
    }

}
