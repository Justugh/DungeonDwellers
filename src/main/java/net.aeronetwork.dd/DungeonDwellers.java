package net.aeronetwork.dd;

import club.encast.enflow.Enflow;
import club.encast.enflow.game.load.balancer.ConnectionLoadBalancer;
import club.encast.enflow.game.load.impl.JoinLoad;
import club.encast.enflow.game.load.impl.LeaveLoad;
import net.aeronetwork.dd.game.DDGame;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class DungeonDwellers extends JavaPlugin {

    public static DungeonDwellers INSTANCE;

    @Override
    public void onEnable() {
        INSTANCE = this;

        new BukkitRunnable() {
            @Override
            public void run() {
                Enflow.GAME_MANAGER.startGame(DDGame.class);

                Enflow.GAME_MANAGER.setLoadBalancer(new ConnectionLoadBalancer() {
                    @Override
                    public void onJoin(JoinLoad joinLoad) {
                        joinLoad.setGame(Enflow.GAME_MANAGER.getGameHandlers().get(0).getGame());
                    }

                    @Override
                    public void onLeave(LeaveLoad leaveLoad) {

                    }
                });
            }
        }.runTaskLater(this, 5 * 20);
    }

    @Override
    public void onDisable() {

    }
}
